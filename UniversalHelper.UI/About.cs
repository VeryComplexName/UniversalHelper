﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UniversalHelper.ApiClient;
using UniversalHelper.Common;
using UniversalHelper.Common.EventArguments;
using UniversalHelper.ProcessingEvents;

namespace UniversalHelper.UI
{
    public partial class About : Form, IAbout
    {
        public MyEvents myEvents = new MyEvents();
        public Main main = null;
        public MyClient client = null;

        public About()
        {
            InitializeComponent();
        }

        private void About_Shown(object sender, EventArgs e)
        {
            main = new Main(myEvents);
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            Hide();
            myEvents.FireLogicStart(this, new LogicStartEventArgs() { events = myEvents, timer = timer1, main = main, client = client });
            main.ShowDialog();
        }

        public void CloseApp()
        {
            Close();
        }
    }
}
