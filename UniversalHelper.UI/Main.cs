﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UniversalHelper.Common;
using UniversalHelper.Common.EventArguments;
using UniversalHelper.ProcessingEvents;

namespace UniversalHelper.UI
{
    public partial class Main : Form, IMain
    {
        public MyEvents myEvents = null;

        public Main(MyEvents events)
        {
            myEvents = events;
            InitializeComponent();
        }

        private void Main_Shown(object sender, EventArgs e)
        {
            textBox1.Text = "";

        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e) => myEvents.FireLogicStop(this, null);

        private void Button1_Click(object sender, EventArgs e) => myEvents.FireLogicCloneRepo(this, new LogicCloneRepoArgs() { 
            ui = this, 
            events = myEvents, 
            repoName = "OpenDoc", 
            localFolder = @"c:\temp\222" 
        });

        public void Msg(string Msg)
        {
            textBox1.Text = Msg;
        }
    }
}
