﻿using System;
using System.Collections.Generic;
using System.Web.Http.Dependencies;
using Microsoft.Extensions.DependencyInjection;

namespace UniversalHelper.Web.Api
{
    /// <summary>
    /// Provides the default dependency resolver for the application - based on IDependencyResolver, which hhas just two methods.
    /// This is combined dependency resolver for MVC and WebAPI usage.
    /// </summary>
    internal class MyDependencyResolver : System.Web.Mvc.IDependencyResolver, IDependencyResolver
    {
        protected IServiceProvider serviceProvider;
        protected IServiceScope scope = null;

        public MyDependencyResolver(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public MyDependencyResolver(IServiceScope scope)
        {
            this.scope = scope;
            this.serviceProvider = scope.ServiceProvider;
        }

        public IDependencyScope BeginScope()
        {
            return new MyDependencyResolver(serviceProvider.CreateScope());
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            scope?.Dispose();
        }

        public object GetService(Type serviceType)
        {
            return this.serviceProvider.GetService(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return this.serviceProvider.GetServices(serviceType);
        }
    }
}
