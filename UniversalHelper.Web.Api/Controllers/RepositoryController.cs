﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Management.Automation;
using System.Web.Http;
//using System.Web.Mvc;
using UniversalHelper.Common;
using UniversalHelper.Ps;
using Newtonsoft.Json;
using UniversalHelper.Web.Api.Models;
using System;
using UniversalHelper.Web.Api.Common;
using System.IO;
using UniversalHelper.Common.Model;
using System.Configuration;
using System.Reflection;
using UniversalHelper.Web.Api.Cfg;
using System.Linq;

namespace UniversalHelper.Web.Api
{
    public class RepositoryController : ApiController
    {
        IRepo _repo = null;
        IWebApiCfgWeb _webApiCfgWeb = null;
        IWebApiCfgApp _webApiCfgApp = null;
        const string ControllerName = "Repository";

        public RepositoryController(IWebApiCfgWeb webApiCfgWeb, IWebApiCfgApp webApiCfgApp, IRepo repo) {
            _repo = repo;
            _webApiCfgWeb = webApiCfgWeb;
            _webApiCfgApp = webApiCfgApp;
        }

        // GET api/v1/repository/myRepo/myBranch
        // 1. Download repo
        // 2. Switch selected branch
        // 3. Zip repo
        // 4. Send archive back
        [HttpGet]
        public System.Web.Http.Results.JsonResult<MyStatus> Get(string version, string name, string branch)
        {
            MyStatus result = new MyStatus();

            if (!VersionTemplateInUrl.Check(ControllerName, version)) { throw new Exception($"Api version has incorrect format [{version}]"); }

            try
            {
                var repo = _webApiCfgWeb.Repositories(name);
                if (repo == null) { throw new Exception($"There is no such repository name known as [{name}]"); }

                result = _repo.CloneSwitchCompressAndBase64(repo.Url, _webApiCfgWeb.GetWorkingFolder(), branch, _webApiCfgWeb.GetArcPath());
            }
            catch (Exception ex)
            {
                result.Set(Constants.MyStatusRc.ERROR, ex.Message, null);
            }
            return Json(result);
        }

        // GET api/v1/repository/myRepo
        // 1. Download repo
        // 2. Zip repo
        // 3. Send archive back
        [HttpGet]
        //[ActionName("Repo")]
        public System.Web.Http.Results.JsonResult<MyStatus> Get(string version, string name)
        {
            MyStatus result = new MyStatus();

            if (!VersionTemplateInUrl.Check(ControllerName, version)) { throw new Exception($"Api version has incorrect format [{version}]"); }

            try
            {
                var repo = _webApiCfgWeb.Repositories(name);
                if (repo == null) { throw new Exception($"There is no such repository name known as [{name}]"); }

                result = _repo.CloneCompressAndBase64(repo.Url, _webApiCfgWeb.GetWorkingFolder(), _webApiCfgWeb.GetArcPath());
            }
            catch (Exception ex)
            {                
                result.Set(Constants.MyStatusRc.ERROR, ex.Message, null);
            }
            return Json(result);
        }

        // GET api/v1/repository
        // Return list of available repositories with their settings
        [HttpGet]
        public System.Web.Http.Results.JsonResult<MyStatus> Get(string version)
        {
            MyStatus result = new MyStatus();
            if (!VersionTemplateInUrl.Check(ControllerName, version)) { throw new Exception($"Api version has incorrect format [{version}]"); }

            try
            {
                var cfg = _webApiCfgWeb.Cfg();

                var repo = _webApiCfgWeb.Repositories();
                if (repo == null) { throw new Exception($"There are no Repositories configured"); }

                result.Data = repo;
            }
            catch (Exception ex)
            {
                result.Set(Constants.MyStatusRc.ERROR, ex.Message, null);
            }
            finally { }

            return Json(result);
        }

        [HttpPost]
        [ActionName("DefaultApi")]
        public System.Web.Http.Results.JsonResult<MyStatus> Post([FromBody] string someString)
        {
            MyStatus result = new MyStatus();


            return Json(result);
        }

        [HttpPost]
        [ActionName("DefaultApi")]
        public System.Web.Http.Results.JsonResult<MyStatus> Put(int name, [FromBody] string someString)
        {
            MyStatus result = new MyStatus();


            return Json(result);
        }

        [HttpDelete]
        [ActionName("DefaultApi")]
        public System.Web.Http.Results.JsonResult<MyStatus> Delete(int name)
        {
            MyStatus result = new MyStatus();


            return Json(result);
        }
    }
}