﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalHelper.Web.Api.Common
{
    public static class VersionTemplateInUrl
    {
        
        public static int GetApiVersion(string ApiVersion)
        {
            int result = 0;

            if (ApiVersion.StartsWith("v"))
            {
                bool ok = int.TryParse(ApiVersion.Substring(1), out result);
                if (!ok) result = 0;
            }

            return result;
        }

        public static bool Check(string ControllerName, string ApiVersion)
        {
            int tmp = GetApiVersion(ApiVersion);
            bool result = false;

            // We support jst v1 of API
            switch (ControllerName.ToLower()) {
                case "repository" : { result = tmp == 1; break; }
                case "action" : { result = tmp == 1; break; }
            }

            return result;
        }
    }
}
