﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Management.Automation;
using System.Text;
using System.Threading.Tasks;
using UniversalHelper.Common;
using UniversalHelper.Common.Model;
using UniversalHelper.Ps;

namespace UniversalHelper.Web.Api.Common
{
    public class Repo : RepoBase, IRepo
    {
        IPowershell _ps;
        Dictionary<string, string> _scripts;

        public Repo(IPowershell ps)
        {
            _ps = ps;
            _scripts = new Dictionary<string, string>();
            _scripts.Add("clone", "cd '{local}' | Out-Null; try{ & git clone {remote} } catch{}; $LASTEXITCODE;");
            _scripts.Add("switch", "cd '{local}' | Out-Null; try{ $tmp = & git rev-parse --abbrev-ref HEAD; if($tmp -ne '{branch}') { & git switch '{branch}' } } catch{}; $LASTEXITCODE;");
        }

        public MyStatus Clone(string repoPath, string localPath)
        {
            MyStatus result = new MyStatus();

            try
            {
                var pso = _ps.Execute(_scripts["clone"].Replace("{local}", localPath).Replace("{remote}", repoPath));

                if (pso.Properties[Constants.PSRetObjNames.Result].Value == null | ((Collection<PSObject>)pso.Properties[Constants.PSRetObjNames.Result].Value).Count != 1)
                {
                    throw new Exception("Something went wrong while repository clonning");
                }

                if (pso.Properties[Constants.PSRetObjNames.Result].Value != null)
                {
                    foreach (PSObject p in (Collection<PSObject>)pso.Properties[Constants.PSRetObjNames.Result].Value)
                    {
                        if (p.BaseObject.GetType() != typeof(System.Int32)) { throw new Exception("Unknown type returned. Should be [System.Int32]"); }
                        if ((int)p.BaseObject != 0) { throw new Exception($"Returned [{(int)p.BaseObject}] code"); }
                        result.Set(Constants.MyStatusRc.OK, "Ok", null);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Set(Constants.MyStatusRc.ERROR, ex.Message, null);
            }
            finally
            {
            }
            return result;
        }

        public MyStatus Switch(string localPath, string branch)
        {
            MyStatus result = new MyStatus();

            try
            {
                var pso = _ps.Execute(_scripts["switch"].Replace("{local}", localPath).Replace("{branch}", branch));

                if (pso.Properties[Constants.PSRetObjNames.Result].Value == null | ((Collection<PSObject>)pso.Properties[Constants.PSRetObjNames.Result].Value).Count != 1)
                {
                    throw new Exception("Something went wrong while switching branch");
                }

                if (pso.Properties[Constants.PSRetObjNames.Result].Value != null)
                {
                    foreach (PSObject p in (Collection<PSObject>)pso.Properties[Constants.PSRetObjNames.Result].Value)
                    {
                        if (p.BaseObject.GetType() != typeof(System.Int32)) { throw new Exception("Unknown type returned. Should be [System.Int32]"); }
                        if ((int)p.BaseObject != 0) { throw new Exception($"Returned [{(int)p.BaseObject}] code"); }
                        result.Set(Constants.MyStatusRc.OK, "Ok", null);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Set(Constants.MyStatusRc.ERROR, ex.Message, null);
            }

            return result;
        }
        
        public MyStatus CloneAndCompress(string repoPath, string localDir, string arcPath, bool delRepoDir = true)
        {
            MyStatus result = new MyStatus();
            string repoFolder = null;

            try
            {
                var tmp = Clone(repoPath, localDir);
                if (tmp.Rc != 0) { throw new Exception(tmp.Msg); }

                var uri = new Uri(repoPath);
                repoFolder = Path.Combine(localDir, Path.GetFileNameWithoutExtension(uri.LocalPath));

                tmp = Compress(repoFolder, arcPath);
                if (tmp.Rc != 0) { throw new Exception(tmp.Msg); }
            }
            catch (Exception ex)
            {
                result.Set(Constants.MyStatusRc.ERROR, ex.Message, null);
            }
            finally
            {
                if (delRepoDir) WipeDir(repoFolder);
            }
            return result;
        }

        public MyStatus CloneCompressAndBase64(string repoPath, string localDir, string arcPath, bool wipeLocalDir = true, bool wipeArc = true)
        {
            MyStatus result = new MyStatus();
            string repoFolder = null;

            try
            {
                var tmp = Clone(repoPath, localDir);
                if (tmp.Rc != 0) { throw new Exception(tmp.Msg); }

                var uri = new Uri(repoPath);
                repoFolder = Path.Combine(localDir, Path.GetFileNameWithoutExtension(uri.LocalPath));

                string base64String = Compression.ZipFolderAndBase64(repoFolder, arcPath, true);
                result.Set(Constants.MyStatusRc.OK, "Ok", base64String);
            }
            catch (Exception ex)
            {
                result.Set(Constants.MyStatusRc.ERROR, ex.Message, null);
            }
            finally
            {
                if (wipeLocalDir) WipeDir(repoFolder);
                if(wipeArc) File.Delete(arcPath);
            }
            return result;
        }

        public MyStatus CloneSwitchCompressAndBase64(string repoPath, string localDir, string branch, string arcPath, bool wipeLocalDir = true, bool wipeArc = true)
        {
            MyStatus result = new MyStatus();
            string repoFolder = null;

            try
            {
                var tmp = Clone(repoPath, localDir);
                if (tmp.Rc != 0) { throw new Exception(tmp.Msg); }

                var url = new Uri(repoPath);
                string scriptsRepoFolder = Path.Combine(localDir, Path.GetFileNameWithoutExtension(url.LocalPath));
                tmp = Switch(scriptsRepoFolder, branch);
                if (tmp.Rc != 0) { throw new Exception(tmp.Msg); }

                var uri = new Uri(repoPath);
                repoFolder = Path.Combine(localDir, Path.GetFileNameWithoutExtension(uri.LocalPath));

                string base64String = Compression.ZipFolderAndBase64(repoFolder, arcPath, true);
                result.Set(Constants.MyStatusRc.OK, "Ok", base64String);
            }
            catch (Exception ex)
            {
                result.Set(Constants.MyStatusRc.ERROR, ex.Message, null);
            }
            finally
            {
                if (wipeLocalDir) WipeDir(repoFolder);
                if (wipeArc) File.Delete(arcPath);
            }
            return result;
        }
    }
}
