﻿using UniversalHelper.Common;
using UniversalHelper.Common.Model;

namespace UniversalHelper.Web.Api.Common
{
    public interface IRepo : IRepoBase
    {
        MyStatus Clone(string repoPath, string localPath);
        MyStatus Switch(string localPath, string branch);
        MyStatus CloneAndCompress(string repoPath, string localDir, string arcPath, bool wipeLocalDir = true);
        MyStatus CloneCompressAndBase64(string repoPath, string localDir, string arcPath, bool wipeLocalDir = true, bool wipeArc = true);
        MyStatus CloneSwitchCompressAndBase64(string repoPath, string localDir, string branch, string arcPath, bool wipeLocalDir = true, bool wipeArc = true);
    }
}