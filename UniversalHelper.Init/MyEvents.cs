﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniversalHelper.Common.EventArguments;

namespace UniversalHelper.ProcessingEvents
{
    public class MyEvents
    {
        // Event Delegates Definition
        public delegate void LogicStartHandler(object sender, LogicStartEventArgs e);
        public delegate void LogicStopHandler(object sender, LogicStopEventArgs e);
        public delegate void LogicCloneRepoHandler(object sender, LogicCloneRepoArgs e);

        // Events Definition
        public event LogicStartHandler LogicStart;
        public event LogicStopHandler LogicStop;
        public event LogicCloneRepoHandler LogicCloneRepo;

        // Event Handlers
        EventHandlers eventHandlers = null;

        public MyEvents() {
            // Event Handlers
            eventHandlers = new EventHandlers();
            // Add Handlers to Events
            LogicStart += eventHandlers.StartWork;
            LogicStop += eventHandlers.StopWork;
            LogicCloneRepo += eventHandlers.CloneRepo;
        }

        // Fire Events
        public void FireLogicStart(Object sender, LogicStartEventArgs e) 
        {
            if (LogicStart != null) { LogicStart(sender, e); }
        }
        public void FireLogicStop(Object sender, LogicStopEventArgs e)
        {
            if (LogicStop != null) { LogicStop(sender, e); }
        }
        public void FireLogicCloneRepo(Object sender, LogicCloneRepoArgs e)
        {
            if (LogicCloneRepo != null) { LogicCloneRepo(sender, e); }
        }


    }
}
