﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalHelper.Common.Model
{
    public enum AvailableAction
    {
        InitialSet,
        RepositoryList,
        CloneRepository
    }

    public class MyClientStatus : MyStatus
    {
        public AvailableAction action;

        public MyClientStatus() : base()
        {
        }

        public MyClientStatus(MyStatus myStatus)
        {
            Set(myStatus.Rc, myStatus.Msg, myStatus.Data, AvailableAction.InitialSet);
        }

        public MyClientStatus(MyStatus myStatus, AvailableAction action = AvailableAction.CloneRepository)
        {
            Set(myStatus.Rc, myStatus.Msg, myStatus.Data, action);
        }

        public MyClientStatus(Constants.MyStatusRc rc = Constants.MyStatusRc.OK, string msg = "", object data = null, AvailableAction action = AvailableAction.CloneRepository) : base(rc, msg, data)
        {
        }

        public void Set(Constants.MyStatusRc rc = Constants.MyStatusRc.OK, string msg = "", object data = null, AvailableAction action = AvailableAction.CloneRepository)
        {
            base.Set(rc, msg, data);
            this.action = action;
        }
    }
}
