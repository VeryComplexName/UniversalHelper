﻿using UniversalHelper.Common.Model;

namespace UniversalHelper.Common
{
    public interface IRepoBase
    {
        void WipeDir(string dir);
        MyStatus CompressFile(string[] srcFile, string arcPath, bool wipeSrcFile = false);
        MyStatus CompressFileAndBase64(string[] srcFile, string arcPath, bool wipeSrcFile = false, bool wipeArc = true);
        MyStatus CompressFile(string srcFile, string arcPath, bool wipeSrcFile = false);
        MyStatus CompressFileAndBase64(string srcFile, string arcPath, bool wipeSrcFile = false, bool wipeArc = true);
        MyStatus Compress(string srcDir, string arcPath, bool wipeSrcDir = true);

        MyStatus DecompressFile(string arcPath, string dstDir, bool wipeArc = false);
        MyStatus Unbase64AndDecompress(string base64string, string dstDir);
    }
}