﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Compression;
using System.IO;

namespace UniversalHelper.Common
{
    public static class Compression
    {
        public static bool Unzip(string arcPath, string destDir, bool wipeArc = false)
        {
            if (!Directory.Exists(destDir)) { if (Directory.CreateDirectory(destDir) == null) { return false; } }
            ZipFile.ExtractToDirectory(arcPath, destDir);
            if(wipeArc) File.Delete(arcPath);
            return true;
        }

        public static bool Unbase64AndUnzip(string base64string, string destDir)
        {
            if (!Directory.Exists(destDir)) { if (Directory.CreateDirectory(destDir) == null) { return false; } }

            byte[] tmp = Convert.FromBase64String(base64string);
            MemoryStream ms = new MemoryStream();
            ms.Write(tmp, 0, tmp.Length);

            ZipArchive archive = new ZipArchive(ms);
            foreach (ZipArchiveEntry entry in archive.Entries)
            {
                string filePath = Path.Combine(destDir, entry.FullName);
                var fileDir = Path.GetDirectoryName(filePath);
                if (!Directory.Exists(fileDir)) { if (Directory.CreateDirectory(fileDir) == null) { return false; } }

                if(!filePath.EndsWith("/")) entry.ExtractToFile(filePath, true);
            }
            return true;
        }

        public static bool Zip(string[] sourceFile, string destFilePath)
        {
            foreach (var f in sourceFile) { if (!File.Exists(f)) { return false; } }

            var dir = Path.GetDirectoryName(destFilePath);
            if (!Directory.Exists(dir)) { if (Directory.CreateDirectory(dir) == null) { return false; } }

            using (var zip = ZipFile.Open(destFilePath, ZipArchiveMode.Create))
                foreach (var f in sourceFile) { zip.CreateEntryFromFile(f, Path.GetFileName(f)); }
            return true;
        }

        public static string ZipAndBase64(string[] sourceFile, string destFilePath, bool removeArcFile = false)
        {
            string base64String = "";
            try
            {
                Zip(sourceFile, destFilePath);
                byte[] content = File.ReadAllBytes(destFilePath);
                base64String = Convert.ToBase64String(content, 0, content.Length);
            }
            finally
            {
                if (removeArcFile)
                {
                    if (File.Exists(destFilePath))
                    {
                        File.SetAttributes(destFilePath, FileAttributes.Normal);
                        File.Delete(destFilePath);
                    }
                }
            }
            return base64String;
        }

        public static bool Zip(string sourceFile, string destFilePath)
        {
            if (!File.Exists(sourceFile)) { return false; }

            var dir = Path.GetDirectoryName(destFilePath);
            if (!Directory.Exists(dir)) { if (Directory.CreateDirectory(dir) == null) { return false; } }

            using (var zip = ZipFile.Open(destFilePath, System.IO.Compression.ZipArchiveMode.Create))
                zip.CreateEntryFromFile(sourceFile, Path.GetFileName(sourceFile));
            return true;
        }

        public static string ZipAndBase64(string sourceFile, string destFilePath, bool removeArcFile = false)
        {
            string base64String = "";
            try
            {
                Zip(sourceFile, destFilePath);
                byte[] content = File.ReadAllBytes(destFilePath);
                base64String = Convert.ToBase64String(content, 0, content.Length);
            }
            finally
            {
                if (removeArcFile)
                {
                    if (File.Exists(destFilePath))
                    {
                        File.SetAttributes(destFilePath, FileAttributes.Normal);
                        File.Delete(destFilePath);
                    }
                }
            }
            return base64String;
        }

        public static bool ZipFolder(string sourceDir, string destFilePath)
        {
            if(!Directory.Exists(sourceDir)) { return false; }

            var dir = System.IO.Path.GetDirectoryName(destFilePath);            
            if (!Directory.Exists(dir)) {if (Directory.CreateDirectory(dir) == null) { return false; }}

            ZipFile.CreateFromDirectory(sourceDir, destFilePath);
            return true;
        }

        public static string ZipFolderAndBase64(string sourceDir, string destFilePath, bool removeArcFile = false)
        {
            string base64String = "";
            try
            {
                Compression.ZipFolder(sourceDir, destFilePath);
                byte[] content = File.ReadAllBytes(destFilePath);
                base64String = Convert.ToBase64String(content, 0, content.Length);
            }
            finally
            {
                if(removeArcFile)
                {
                    if(File.Exists(destFilePath)) {
                        File.SetAttributes(destFilePath, FileAttributes.Normal);
                        File.Delete(destFilePath); 
                    }
                }
            }
            return base64String;
        }
    }
}
