﻿namespace UniversalHelper.Web.Api.Cfg
{
    public interface IWebApiCfgApp
    {
        string Get(string Name);
    }
}