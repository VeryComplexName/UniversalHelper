﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using GitLabApiClient;
using Newtonsoft.Json;
using UniversalHelper.Common.Model;
using UniversalHelper.Web.Api.Common;

namespace UniversalHelper.Web.Api.Cfg
{
    public class WebApiCfgWeb : IWebApiCfgWeb, IDisposable
    {
        private IWebApiCfgApp _cfgApp;
        private IRepo _repo;
        private MyWebApiCfg cfg;

        public WebApiCfgWeb(IWebApiCfgApp cfgApp, IRepo repo)
        {
            _cfgApp = cfgApp;
            _repo = repo;

            cfg = Cfg(true);
        }

        public void Dispose()
        {
            try {if (!string.IsNullOrEmpty(WorkingFolder) & Directory.Exists(WorkingFolder)) Directory.Delete(WorkingFolder, true);} catch { }
        }

        private void CreateWorkingFolder() => workingFolder = Directory.CreateDirectory(Path.Combine(Path.GetTempPath(), _cfgApp.Get("webServerWorkingDirName"), Path.GetRandomFileName())).FullName;

        public string GetScriptsFolderName() { return "scripts"; }
        public string GetIconsFolderName() { return "icons"; }

        public string GetModulesFolderName() { return "modules"; }

        // WorkingFolder
        private string workingFolder;
        public string WorkingFolder { get { if (string.IsNullOrEmpty(workingFolder)) { CreateWorkingFolder(); } return workingFolder; } } // set => workingFolder = value; }
        public string GetWorkingFolder() => WorkingFolder;

        // ScriptsRepoFolder
        private string scriptsRepoFolder;
        public string ScriptsRepoFolder { 
            get { 
                if (string.IsNullOrEmpty(scriptsRepoFolder)) {
                    var tmp = new Uri(_cfgApp.Get("webServerScriptsRepo"));
                    scriptsRepoFolder = Path.Combine(WorkingFolder, Path.GetFileNameWithoutExtension(tmp.LocalPath));
                }
                return scriptsRepoFolder;
            } 
        }
        public string GetScriptsRepoFolder() => ScriptsRepoFolder;
        public bool IsScriptRepoFolderExists() => Directory.Exists(ScriptsRepoFolder);

        // Archive Name
        private string arcPath;
        public string ArcPath
        {
            get {
                if (string.IsNullOrEmpty(arcPath)) { arcPath = Path.Combine(WorkingFolder, Path.GetRandomFileName()); }
                if (File.Exists(arcPath)) { File.Delete(arcPath); }
                return arcPath; 
            }
        }
        public string GetArcPath() => ArcPath;

        // Cfg
        public MyWebApiCfg DefaultCfg()
        {
            MyWebApiCfg cfg = new MyWebApiCfg();
            cfg.Repositories = new List<MyRepo>();
            cfg.Repositories.Add(new MyRepo() { Name = "Office2Md", Url = @"https://gitlab.com/VeryComplexName/opendoc.git", Branch = "main" });
            cfg.Repositories.Add(new MyRepo() { Name = "OpenDoc", Url = @"https://gitlab.com/VeryComplexName/opendoc.git", Branch = "main" });
            cfg.Repositories.Add(new MyRepo() { Name = "UH", Url = @"https://gitlab.com/VeryComplexName/uhscripts.git", Branch = "main" });
            cfg.Repositories.Add(new MyRepo() { Name = "UHScripts", Url = @"https://gitlab.com/VeryComplexName/uhscripts.git", Branch = "main" });

            cfg.Actions = new List<MyPsAction>();
            cfg.Actions.Add(new MyPsAction() { Name = "Run VSCode", Parameters = null, Script = @"& VSCode.exe" });

            return cfg;
        }

        public MyWebApiCfg ReadCfg()
        {
            /*
            string assemblyFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var cfgName = ConfigurationManager.AppSettings["webServerCfgName"];
            string pathToCfg = Path.Combine(assemblyFolder, Path.GetFileName(cfgName));
            */

            string cfgUrl = _cfgApp.Get("webServerScriptsRepo");
            if (string.IsNullOrEmpty(cfgUrl)) throw new Exception("There is no [webServerScriptsRepo] configured in app.config");

            var status = _repo.Clone(cfgUrl, WorkingFolder);
            if (status.Rc != 0) throw new Exception($"Error [{status.Msg}] during getting config from [{cfgUrl}]");

            string cfgName = _cfgApp.Get("webServerCfgName");
            if (IsScriptRepoFolderExists() & !string.IsNullOrEmpty(cfgName))
            {
                string pathToCfg = Path.Combine(ScriptsRepoFolder, cfgName);
                if (!File.Exists(pathToCfg)) throw new Exception($"There is no configuration file [{cfgName}] defined in repository [{cfgUrl}]");
                
                string jsonString = File.ReadAllText(pathToCfg);
                return JsonConvert.DeserializeObject<MyWebApiCfg>(jsonString);
            }
            else
            {
                return DefaultCfg();
            }
        }

        public MyWebApiCfg Cfg(bool reRead = false, bool clone = true)
        {
            if (reRead) cfg = null;

            if (cfg == null) cfg = ReadCfg();
            if (clone) return (MyWebApiCfg)Clone(cfg);
            return cfg;
        }

        public List<MyRepo> Repositories(bool clone = true)
        {
            if (clone) return (List<MyRepo>)Clone(cfg.Repositories);
            return cfg.Repositories;
        }
        public MyRepo Repositories(string Name, bool clone = true)
        {
            if (cfg.Repositories != null)
            {
                var tmp = cfg.Repositories.Where(x => string.Compare(x.Name, Name, true) == 0).FirstOrDefault();
                if (clone) return (MyRepo)Clone(tmp);
                return tmp;
            }
            else return null;
        }

        public List<MyPsAction> Actions(bool clone = true)
        {
            if (clone) return (List<MyPsAction>)Clone(cfg.Actions);
            return cfg.Actions;
        }
        public MyPsAction Actions(string Name, bool clone = true)
        {
            if (cfg.Actions != null)
            {
                var tmp = cfg.Actions.Where(x => string.Compare(x.Name, Name, true) == 0).FirstOrDefault();
                if (clone) return (MyPsAction)Clone(tmp);
                return tmp;
            }
            else return null;
        }

        public object Clone(object cloneFrom)
        {
            MemoryStream ms = new MemoryStream();
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(ms, cloneFrom);
            ms.Position = 0;
            object obj = bf.Deserialize(ms);
            ms.Close();
            return obj;
        }

        public MyWebApiCfg CfgZipAndBase64()
        {
            var tmp = Cfg(); // Clonned
            tmp.Actions = ActionsZipAndBase64(tmp.Actions);

            return tmp;
        }

        public List<MyPsAction> ActionsZipAndBase64(List<MyPsAction> actions)
        {
            actions.ForEach(x => ActionsZipAndBase64(x));
            return actions;
        }

        public MyPsAction ActionsZipAndBase64(MyPsAction action)
        {
            // Zip & Compress Actions: Script, Modules & Icons
            string scriptsPath = Path.Combine(GetScriptsRepoFolder(), GetScriptsFolderName());

            // --- Script ---
            string currentScript = Path.Combine(scriptsPath, action.Script);
            if (File.Exists(currentScript))
            {
                var ret = _repo.CompressFileAndBase64(currentScript, GetArcPath());
                if (ret.Rc != 0) { throw new Exception($"There is error during work with file [{currentScript}]"); }
                action.Script = ret.Data.ToString();
            }
            else
            {
                byte[] content = Encoding.ASCII.GetBytes(action.Script);
                action.Script = Convert.ToBase64String(content, 0, content.Length);
            }

            // --- Icon ---
            string iconsPath = Path.Combine(GetScriptsRepoFolder(), GetIconsFolderName());
            string currentIcon = Path.Combine(iconsPath, action.Icon);
            if (!File.Exists(currentIcon)) { throw new Exception($"No iсon found [{currentIcon}]"); }
            var tmp = _repo.CompressFileAndBase64(currentIcon, GetArcPath());
            if (tmp.Rc != 0) { throw new Exception($"There is error during work with file [{currentIcon}]"); }
            action.Icon = tmp.Data.ToString();

            // --- Modules ---
            string modulesPath = Path.Combine(GetScriptsRepoFolder(), GetModulesFolderName());
            foreach (var module in action.Modules)
            {
                string currentModule = Path.Combine(modulesPath, module.Content);
                if (File.Exists(currentModule))
                {
                    var ret = _repo.CompressFileAndBase64(currentModule, GetArcPath());
                    if (ret.Rc != 0) { throw new Exception($"There is error during work with file [{currentModule}]"); }
                    module.Content = ret.Data.ToString();
                }
                else
                {
                    byte[] content = Encoding.ASCII.GetBytes(module.Content);
                    module.Content = Convert.ToBase64String(content, 0, content.Length);
                }
            }
            return action;
        }

        public string RepositoryZipAndBase64(MyRepo repo)
        {
            var tmp = _repo.CloneCompressAndBase64(repo.Url, GetWorkingFolder(), GetArcPath());
            if (tmp.Rc != 0) { throw new Exception($"There is error during compress/mime Repository [{repo.Name}]"); }
            return tmp.Data.ToString();
        }
    }
}
